<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::dropIfExists('doctor_schedule');
      Schema::create('doctor_schedule', function($t){
          $t->increments('doctor_schedule_id');
          $t->integer('personstocater')->unsigned();
          $t->date('date');
          $t->string('clinic_start', 50);
          $t->string('clinic_end', 50);
          $t->integer('doctor_id')->unsigned();
          $t->integer('patient_id')->unsigned();
      });

      // Schema::table('doctor_schedule', function($t) {
      //     $t->foreign('doctor_id')->references('doctor_id')->on('doctor')->onDelete('cascade');
      //     $t->foreign('patient_id')->references('patient_id')->on('patient')->onDelete('cascade');
      // });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctor_schedule');
    }
}
