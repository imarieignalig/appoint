<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::dropIfExists('doctor');
      Schema::create('doctor', function($t){
          $t->increments('doctor_id');
          $t->string('specialization', 50);
          $t->integer('person_id')->unsigned();
      });

     //  Schema::table('doctor', function($t) {
     //   $t->foreign('person_id')->references('person_id')->on('person')->onDelete('cascade');
     // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctor');
    }
}
