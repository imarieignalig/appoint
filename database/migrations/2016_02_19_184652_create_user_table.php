<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::dropIfExists('user');
      Schema::create('user', function($t){
          $t->increments('user_id');
          $t->string('usertype', 50);
          $t->string('username', 50);
          $t->string('password', 50);
          $t->integer('person_id')->unsigned();
      });

     //  Schema::table('user', function($t) {
     //    $t->foreign('person_id')->references('person_id')->on('person')->onDelete('cascade');
     // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
    }
}
