<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::dropIfExists('person');
      Schema::create('person', function($t){
          $t->increments('person_id');
          $t->string('first_name', 50);
          $t->string('middle_name', 50);
          $t->string('last_name', 50);
          $t->string('birthdate', 50);
          $t->string('gender', 50);
          $t->string('address', 200);
          $t->string('email_add', 50);
          $t->string('religion', 50);
      });

     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('person');
    }
}
