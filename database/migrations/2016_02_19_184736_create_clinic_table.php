<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::dropIfExists('clinic');
      Schema::create('clinic', function($t){
          $t->increments('clinic_id');
          $t->string('clinic_name', 50);
          $t->string('building_name', 50);
          $t->string('location', 50);
          $t->integer('room_no')->unsigned();
          $t->integer('floor_no')->unsigned();
          $t->integer('doctor_id')->unsigned();
      });

    // Schema::table('clinic', function($t) {
    //     $t->foreign('doctor_id')->references('doctor_id')->on('doctor')->onDelete('cascade');
    // });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clinic');
    }
}
