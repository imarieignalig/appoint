<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('patient');
        Schema::create('patient', function ($t)
        {
          $t->increments('patient_id');
          $t->integer('person_id')->unsigned();
        });

        // Schema::table('patient', function($t) {
        //     $t->foreign('person_id')->references('person_id')->on('person')->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patient');
    }
}
