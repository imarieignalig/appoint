<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::dropIfExists('schedule');
      Schema::create('schedule', function($t){
          $t->increments('schedule_id');
          $t->string('timeslot_start', 50);
          $t->string('timeslot_end', 50);
          $t->integer('patient_id')->unsigned();
          $t->integer('doctor_schedule_id')->unsigned();
      });

    //     Schema::table('schedule', function($t) {
    //     $t->foreign('patient_id')->references('patient_id')->on('patient')->onDelete('cascade');
    //     $t->foreign('doctor_schedule_id')->references('doctor_schedule_id')->on('doctor_schedule')->onDelete('cascade');
    // });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('schedule');
    }
}
