(function() {
  var app = angular.module('appoint', []);

  app.controller('RegistrationController', function() {
    this.religions = religion;
    this.specialzations = specialzation;
  });

  var religion = ['Roman Catholic', 'Protestant', 'Catholic'];
  var specialzation = ['Dentistry/Oral Surgery','Dermatology','Gynecology','Maternal and Fetal Medicine','Neurology','Ophthalmology','Pediatrics'];
})();
