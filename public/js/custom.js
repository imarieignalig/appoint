$(document).ready(function() {

  $.smoothScroll();

  $("#ap_carousel").owlCarousel({

  navigation : true, // Show next and prev buttons
  slideSpeed : 300,
  paginationSpeed : 400,
  singleItem:true,
  autoPlay: 4000,
  });

  //no-animation if mobile
  setInterval(function(){
    var viewportWidth = $(window).width();
    var sb = $('#search_bar');
    if(viewportWidth < 750){
      sb.addClass('notransition');
    }else{
      sb.removeClass('notransition');
    }
  },50);

  
});
