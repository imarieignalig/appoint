<!DOCTYPE html>
<html ng-app="appoint">
<head>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="css/main.css">
	@yield('title')
</head>

<body>
	@yield('content')

  <script type="text/javascript" src="js/jquery-2.2.0.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.js"></script>
  <script type="text/javascript" src="js/owl.carousel.js"></script>
  <script type="text/javascript" src="js/smoothscroll.js"></script>
  <script type="text/javascript" src="js/custom.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script>
  <script type="text/javascript" src="js/angular/app.js"></script>
</body>
</html>
