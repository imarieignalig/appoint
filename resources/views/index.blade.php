@extends('app')
@section('title')
<title>Appoint</title>
@stop

@section('content')
    <body>
        <div id="header">
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation" aria-expanded="false">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <strong class="navbar-brand" href="#">Appoint</strong>
                    </div>
                    <div class="collapse navbar-collapse" id="navigation">
                        <ul class="nav navbar-nav text-center">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">About</a></li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div id="search_area" class="form-group text-center">
                                <input id="search_bar" type="search" class="form-control" placeholder="Search">
                            </div>
                        </form>
                        <ul class="nav navbar-nav text-center pull-right">
                            <li><a href="#" data-toggle="modal" data-target="#RegisterModal">Sign Up</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#LoginModal">Sign In</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div id="ap_carousel" class="owl-carousel owl-theme">
            <div class="item"><img src="images/image1.jpg"></div>
            <div class="item"><img src="images/image2.jpg"></div>
            <div class="item"><img src="images/image3.jpg"></div>
        </div>
        <div id="about">
            <div id="box_350">
                <div class="row text-center">
                    <div class="title-area">
                        <h2 class="title">Welcome to <span>Appoint</span></h2>
                        <span class="title-line"></span>
                        <p>we are the pabebe boys</p>
                    </div>
                </div>
            </div>
        </div>
        <div id="services" class="container">
            <div class="row text-center">
                <div class="title-area">
                    <h2 class="title">Services we offer</h2>
                    <span class="title-line"></span>
                    <p>perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium totam rem aperiam, eaque ipsa quae ab illo inventore</p>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div id="box_200" class="bg bg_animate">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div id="box_200" class="bg bg_animate">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div id="box_200" class="bg bg_animate">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div id="box_200" class="bg bg_animate">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div id="box_200" class="bg bg_animate">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div id="box_200" class="bg bg_animate">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div id="box_200" class="bg bg_animate">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div id="box_200" class="bg bg_animate">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="footer" class="container">
                <h1>COPYRIGHT © 2014 APPOINT ALL RIGHTS RESERVED</h1>
            </div>
        </div>

        <!-- Modals -->

        <!-- Register -->
        <div id="RegisterModal" class="modal fade" role="dialog" ng-controller="RegistrationController as registration">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Register As</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <section>
                                <ul class="nav nav-pills">
                                    <div class="col-md-6">
                                        <li><a href="#" class="btn btn-info ap-btn-standard" ng-click="tab=1">Patient</a></li>
                                    </div>
                                    <div class="col-md-6">
                                        <li><a href="#" class="btn btn-info ap-btn-standard" ng-click="tab=2">Doctor</a></li>
                                    </div>
                                </ul>
                            </section>
                        </div>
                        <hr>
                        <form action="" method="post">
                            <div id="form" class="form-group">
                                <section ng-show="tab <= 2">
                                    <label for="name">Name</label>
                                    <div id="name" class="row">
                                        <div class="col-md-4 rmv-right-padding">
                                            <input type="text" name="firstname" class="form-control maximize" placeholder="First">
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" name="middlename" class="form-control maximize" placeholder="Middle">
                                        </div>
                                        <div class="col-md-4 rmv-left-padding">
                                            <input type="text" name="lastname" class="form-control maximize" placeholder="Last">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-6 rmv-right-padding">
                                            <label for="name">Gender</label>
                                            <select class="form-control" name="gender">
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="birthdate">Birthdate</label>
                                            <input type="date" name="birthdate" class="form-control" placeholder="Birthdate">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="address">Address</label>
                                            <input type="text" name="address" class="form-control" placeholder="Address">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="religion">Religion</label>
                                            <select ng-model="registration.religion" class="form-control" ng-options='rel for rel in registration.religions'></select>
                                        </div>
                                    </div>
                                </section>
                                <br ng-show="tab===2">
                                <div id="doctor_add" class="row" ng-show="tab===2">
                                    <div class="col-md-12">
                                        <label for="specialization">Specialization</label>
                                        <select ng-model="registration.specialzation" class="form-control" ng-options='spc for spc in registration.specialzations'></select>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div id="modal_footer" class="modal-footer modal-btn-center" ng-show="tab != null">
                    <button type="submit" class="btn btn-primary ap-btn-half">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Register Modal end -->
        <!-- Login Modal -->
        <div id="LoginModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Login</h4>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                          <div class="row">
                              <div class="col-md-12">
                                  <label for="username">Username</label>
                                  <input type="text" name="username" class="form-control" placeholder="Username">
                              </div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-12">
                                  <label for="password">Password</label>
                                  <input type="password" name="password" class="form-control" placeholder="Password">
                              </div>
                          </div>
                        </div>
                        <div id="modal_footer" class="modal-footer modal-btn-center">
                            <button type="submit" class="btn btn-primary ap-btn-half">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Login Modal end -->
@stop
